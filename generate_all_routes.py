#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import random
import json
import pyproj
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd

from functools import partial
from json import JSONEncoder
from datetime import datetime
from statistics import mean
from geopy.distance import geodesic

from matplotlib import cm
from shapely import geometry
from shapely import ops

"""
For experiment purpose, this script list ALL the "routes" that satisfied the constraints
The input file is the original file

1. Read the original data, select useful columns
2. Separate data accroding to trains: Train 1 ~ 4; and MNCs
	save them to separate .csv files
3. Read each file, select routes based on the method provided and save the additional files
	The file format should be "route_x.csv"
"""

# define a continuous interval by start time and end time
# make it easier to compare
class continuous_interval:
	def __init__(self, start_time, end_time):
		self.start_time = start_time
		self.end_time = end_time
	def __str__(self):
		return "<start = %s, end = %s>" %(self.start_time, self.end_time)


if __name__ == '__main__':


	""" input/output file directories """
	input_dir = "dataset/"
	output_dir = "dataset/test_routes/"


	""" parameters """
	gap_in_sec = 10										# define the "gap" in seconds
														# a route is considered continuous if there is no gap

														# we only select routes between "lb" and "ub"" seconds
	total_sec_lb = 60 									# define the time interval lower bound
	total_sec_ub = 300									# define the time interval upper bound

														# limit the "length" of each pci group
	pci_length_lb = 5									# define the lower bound #record of a pci group
	pci_length_ub = 10									# define the upper bound #record of a pci group

	total_pci_lb = 5 									# define the lower bound #pci of a route


	#train_list = ['Train1', 'Train2', 'Train3', 'Train4', 'Train5']
	#mnc_list = [10, 15, 20, 30]

	train_list = ['Train1']
	mnc_list = [10]

	global_valid_route_counter = 0

	for t in train_list:

		for m in mnc_list:

			print ("================ processing data Train = %s, MNC = %s ==================" %(t, str(m)))
			train_mnc_df = pd.read_csv(input_dir+'%s_mnc_%s.csv' %(t, str(m)), usecols=['latitude', 'longitude', 
																					  'speed', 'datetime',
																					  'earfcn', 'dlfreq', 
																					  'pci', 'cal_rsrp', 'rsrq'])
			
			
			# convert datetime column to actual datetime data structure from string
			dates = pd.to_datetime(train_mnc_df.datetime, format='%Y-%m-%d %H:%M:%S')
			train_mnc_df = (train_mnc_df.assign(datetime=dates))
			real_datetime_list = train_mnc_df.datetime.to_list()

			############################### separate the data into continuous intervals #####################
			time_intervals = [] 			# a list of continuous_intervals

			start_time = real_datetime_list[0]

			for index in range(1, len(real_datetime_list)):		# start from the second item
				prev_time = real_datetime_list[index-1]
				current_time = real_datetime_list[index]
				time_diff = current_time-prev_time
				#print ("prev = %s, current = %s, diff in %d sec" %(prev_time, current_time, time_diff.seconds))
				
				if time_diff.total_seconds() > gap_in_sec: 		# X minutes, this can be changed
				
					time_intervals.append(continuous_interval(start_time, prev_time))
					#interval_length_list.append((prev_time-start_time).total_seconds())
					#interval_gap_list.append(time_diff.total_seconds())
					start_time = current_time
					
				else:
					pass

			time_intervals.append(continuous_interval(start_time, real_datetime_list[-1]))


			############################### select the intervals within bound #############################
			chosen_time_intervals = []

			for interval in time_intervals:

				time_difference = interval.end_time-interval.start_time
			
				if (time_difference.total_seconds() > total_sec_lb) and (time_difference.total_seconds() < total_sec_ub):
					chosen_time_intervals.append(interval)

			################ from the chosen intervals, select ones satsify constraints ###########
			for interval in chosen_time_intervals:

				interval_df = train_mnc_df.loc[(train_mnc_df['datetime'] >= interval.start_time) & 
																			 (train_mnc_df['datetime'] <= interval.end_time)]

				# first only keep the rows with one dlfreq
				dlfreq_list = interval_df['dlfreq'].to_list()
				chosen_dlfreq = max(set(dlfreq_list), key=dlfreq_list.count)
				interval_df  = interval_df[interval_df['dlfreq'] == chosen_dlfreq]

				# then start exam the constraints
				satisfied = True

				# constraint 1: the interval must have geo distance > 50 m
				first_row = interval_df.iloc[0]
				last_row = interval_df.iloc[-1]

				if geodesic( (first_row['latitude'], first_row['longitude']),
							 (last_row['latitude'], last_row['longitude']) ).m < 500:
					satisfied = False

				# constraint 2: must contains 2 ~ 3 PCIs
				pci_group = interval_df.groupby(['pci'])
				pci_list = interval_df.pci.unique()

				if (len(pci_list) < pci_length_lb) or (len(pci_list) > pci_length_ub):
					satisfied = False

				# constraint 3: each PCI group must contains at least 5 data points
				for name, group in pci_group:
					if len(group) < 5:
						satisfied = False

				# constraint 4: PCI points must be "non-overlapping"
				seen_pci = []
				prev_pci = interval_df.iloc[0]['pci']
				seen_pci.append(prev_pci)

				for index, row in interval_df.iterrows():
					current_pci = row['pci']
					if current_pci != prev_pci:
						if current_pci in seen_pci:
							satisfied = False
							break
						else:
							seen_pci.append(current_pci)
							prev_pci = current_pci 
					else:
						pass
				

				# if ALL constraints are satisfied, store this interval_df into csv file
				if satisfied:
					print ("this route is valid, save it")
					
					# one more thing, drop the "Day" components, only keep time
					datetimes = interval_df.datetime.to_list()
					datetimes = [d.time() for d in datetimes]
					interval_df = (interval_df.assign(datetime=datetimes))
					#interval_df.to_csv('dataset/valid_routes/route_%d.csv' %(global_valid_route_counter))
					interval_df.to_csv(output_dir+'route_%d.csv' %(global_valid_route_counter), index=False)
					global_valid_route_counter += 1
				else:
					print ("invalid route, skip")
					pass

			# end of constraints check for-loop
		# end of mnc for-loop
	# end of train for-loop












	
	
