#!/usr/bin/python3
import sys, ast, getopt, types
import random
import subprocess
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import geopandas as gpd

from geopy.distance import geodesic
from shapely.geometry import Point, Polygon
from datetime import datetime 

"""
Process the UE file in the following steps:
	1. sort the rows with column "pci" values
	2. sort each "pci bins" by "train", then by "datetime" 
	3. lastly, sort latitude and longitude

	Then, for each "row set" with the same pci + train:
		check the max{datetime} - min{datetime}, if it make sense
		if it spans to multiple "datetime", separate them into row sets

	Then, for each "row set" with the same pci + train + datetime
		check the max{lat, lon} - min{lat, lon}, if it make sense
"""

class eNBInfo:
	def __init__(self, lat_list, lon_list, mnc_list):
		self.lat_list = lat_list
		self.lon_list = lon_list
		self.mnc_list = mnc_list
		self.candidate_list = [] # a list of (lat, lon) tuples represent candidate eNB position	

if __name__ == '__main__':


	# =========================== generate some sample file =======================
	"""
	df = pd.read_csv('lte-jun18tojun19-yt.csv', nrows=3000)
	df = df.sort_values(['pci', 'train', 'datetime'], ascending=True)

	df.to_csv('sample_ue_data.csv')
	"""
	# =============================================================================

	# =============== split original UE file according to trains =============
	"""
	df = pd.read_csv('lte-jun18tojun19-yt.csv')
	train_groups = df.groupby('train')

	for name, group in train_groups:
		
		filename = 'ue_data_%s.csv' %(str(name))

		print ('--------- name = %s ---------' %(str(name)))
		#print (group)

		group.to_csv(filename)
	"""
	# ==============================================================================
	
	# ==================== handle Train 4 as an example ==============================
	df = pd.read_csv('ue_data_Train4.csv', nrows=70000, 
						usecols=['latitude', 'longitude', 'pci', 'datetime', 'mnc', 'speed'])

	# convert string into datetime object, and assign to the original column
	dates = pd.to_datetime(df.datetime, format='%Y-%m-%d %H:%M:%S') 	
	df = (df.assign(datetime=dates))

	eNBInfo_dict = {} 	# key=<pci,day>, value=eNBInfo object

	# then, separate the data into "days"
	pci_hour_groups = df.groupby(['pci', pd.Grouper(key='datetime', freq='H')])


	for name, group in pci_hour_groups:
		
		if len(group != 0):

			#print ("============= name = %s ============" %(str(name)))

			time_interval = group['datetime'].max() - group['datetime'].min()

			max_lat = group['latitude'].max()
			min_lat = group['latitude'].min()
			max_lon = group['longitude'].max()
			min_lon = group['longitude'].min()

			diagonal_distance = geodesic((max_lat, max_lon), (min_lat, min_lon)).km
			#print ("time interval: %s, diagnal = %f km" %(time_interval, diagonal_distance))

			if diagonal_distance < 5:
				lat_list = group['latitude'].tolist()
				lon_list = group['longitude'].tolist()
				mnc_list = list(set(df['mnc']))
				eNBInfo_dict[name] = eNBInfo(lat_list, lon_list, mnc_list)

				#print ("time interval: %s, diagnal = %f km" %(time_interval, diagonal_distance))

	#print (eNBInfo_dict)

	# ================== now scan the tower data and identify eNB ================

	# pick a random dictionary entry

	print ("pick a random element")

	k = random.choice(list(eNBInfo_dict.keys()))
	info = eNBInfo_dict[k]

	print ("this element has lat: %s" %(str(info.lat_list)))
	print ("this element has lon: %s" %(str(info.lon_list)))
	print ("this element has mnc: %s" %(str(info.mnc_list)))

	max_lat = max(info.lat_list)
	min_lat = min(info.lat_list)
	max_lon = max(info.lon_list)
	min_lon = min(info.lon_list)

	df = pd.read_csv('UK_tower_lte.csv', usecols=['net', 'lat', 'lon', 'range'])

	print ("loaded tower data")

	"""
	select the records with the following condition:
		1. the cell should cover at least 3 points
		2. the cell's "net" should be contained in the "mnc list"
	"""

	for index, row in df.iterrows():
		
		coverage = row['range']
		counter = 0

		for point in [(max_lat, max_lon), (max_lat, min_lon), (min_lat, max_lon), (min_lat, min_lon)]:
			if geodesic((row['lat'], row['lon']), point).m <= coverage:
				counter += 1
		
		if counter >= 3:
			#print ("we got a eNB with lat = %s, lon = %s" %(row['lat'], row['lon']))
			if row['net'] in info.mnc_list:
				info.candidate_list.append((row['lat'], row['lon']))
				print ("we got a eNB with lat = %s, lon = %s, mnc = %d" %(row['lat'], row['lon'], row['net']))

	print (len(info.candidate_list))

	"""
	how to store the information?

	"""


	# ================================================================================
	

	# after above processing, we can assume each group corresponds to ONE and ONLY ONE cell
	# then we can start estimate the position (lat, lon) of the cell

	# df.to_csv("ue_measurement_truncate.csv")

	"""

	# first plot each "trip segment" for each <pci, train, time_interval> tuples
	#uk_map = gpd.read_file("./map/gb_1km.shp") 	# import UK map file

	crs = {'init':'epsg:4326'} 		# designate coordinate system
	geometry = [Point(xy) for xy in zip(df['longitude'], df['latitude'])]

	# create geometry dataframe
	# this will insert an additonal column "geometry", where each element is a Point() structure
	geo_df = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)

	#print (geo_df)


	#df.to_csv('ue_measurement_truncate.csv')

	# create figure and axes, assign to subplot
	fig, ax = plt.subplots(figsize=(15,15))
	# add .shp mapfile to axes
	# uk_map.plot(ax=ax, alpha=0.4,color='grey')
	

	# add geodataframe to axes
	# assign ‘price’ variable to represent coordinates on graph
	# add legend
	# make datapoints transparent using alpha
	# assign size of points using markersize
	geo_df.plot(column='pci',ax=ax,alpha=1, legend=True,markersize=1)
	# add title to graph
	plt.title('PCI values for route', fontsize=15,fontweight='bold')
	# set latitiude and longitude boundaries for map display
	plt.xlim(-4.14,-1.34)
	plt.ylim(50.37,52.14)
	# show map
	plt.savefig("UE_path.png")

	"""