#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import random
import json
import pyproj
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd

from functools import partial
from json import JSONEncoder
from datetime import datetime
from statistics import mean
from geopy.distance import geodesic

from matplotlib import cm
from shapely import geometry
from shapely import ops

"""
The logic of this program is introduced in:
	https://docs.google.com/document/d/13z704mtvETE09TB-vwgJPHVX5V0f7gvwYX5-4_dOrjE/edit
"""

"""
Data structure to store the tower information, this will be used to 
select the best tower and output to the csv file
"""
class tower_records:
	def __init__(self, mnc, lat, lon, tower_range):
		self.mnc = mnc
		self.lat = lat
		self.lon = lon
		self.tower_range = tower_range
		self.pci = "0"
	def __str__(self):
		return "<mnc = %s, lat = %s, lon = %s, range = %s, pci = %s>" %(self.mnc, self.lat, self.lon, self.tower_range, self.pci)

class tower_record_encoder(JSONEncoder):
	def default(self, o):
		return o.__dict__

"""
Data structure to define an "appropriate route"
"""
class continuous_interval:
	def __init__(self, start_time, end_time):
		self.start_time = start_time
		self.end_time = end_time
	def __str__(self):
		return "<start = %s, end = %s>" %(self.start_time, self.end_time)

"""
The whole procedure is described in the link, besides select the route and towers,
also draw the figures
"""
if __name__ == '__main__':

	crs = "epsg:4326" 	# designate coordinate system

	gap_in_sec = 2										# define the "gap" in seconds
	total_sec_lb = 10 									# define the interval lower bound
	total_sec_ub = 20									# define the interval upper bound

	#long_interval_in_hours_lower_bound = 0.1			# define the "long time interval" in hours
	#long_interval_in_hours_upper_bound = 0.2 			# define the "long time interval" in hours

	# nrows = 1000000
	df = pd.read_csv('dataset/ue_data_Train3.csv', 
					usecols=['datetime', 'latitude', 'longitude', 'speed', 'pci', 'mnc', 
							 'earfcn', 'dlfreq', 'cal_rsrp', 'total_power', 'rsrq', 'sinr'])

	################################# filter by MNC first ##################################
	mnc_values = df.mnc.unique()
	chosen_mnc = random_mnc = random.choice(mnc_values)

	df = df.loc[df['mnc'] == chosen_mnc]

	################################# separate records by time gap #########################################
	
	# convert datetime to actual datetime
	dates = pd.to_datetime(df.datetime, format='%Y-%m-%d %H:%M:%S')
	df = (df.assign(datetime=dates))

	real_datetime_list = df.datetime.to_list()

	time_intervals = [] 			# a list of continuous_interval
	interval_length_list = [] 		# records the length of each interval, in seconds
	interval_gap_list = []			# records the length of each interval gap, in seconds

	start_time = real_datetime_list[0]

	for index in range(1, len(real_datetime_list)):		# start from the second item
		prev_time = real_datetime_list[index-1]
		current_time = real_datetime_list[index]
		time_diff = current_time - prev_time
		#print ("prev = %s, current = %s, diff in %d sec" %(prev_time, current_time, time_diff.seconds))
		if time_diff.total_seconds() > gap_in_sec: 		# X minutes, this can be changed
		
			#print ("prev = %s, current = %s, diff in %d sec" %(prev_time, current_time, time_diff.total_seconds()))
			# then prev_time is the end_time of last interval
			# current_time is the start_time of the current new interval
			time_intervals.append(continuous_interval(start_time, prev_time))
			interval_length_list.append((prev_time-start_time).total_seconds())
			interval_gap_list.append(time_diff.total_seconds())

			start_time = current_time
			#print ("greater")
		else:
			pass

	# at the end, finish the last interval with the last timestamp
	time_intervals.append(continuous_interval(start_time, real_datetime_list[-1]))
	interval_length_list.append((real_datetime_list[-1]-start_time).seconds)

	"""
	for interval in time_intervals:
		print (interval)
	"""

	""" ======================= what to findout about these intervals? ================
		1. How many intervals in total?
		2. Will any interval "cross-day"?
		3. Min, max, avg of each interval length
		4. Min, max, avg of each interval gap (we only know the gap >= 5 minutes)
	"""
	total_number_intervals = len(time_intervals)
	no_cross_day = True
	for interval in time_intervals:
		if interval.start_time.date() == interval.end_time.date():
			pass
		else:
			print ("there is a cross-day interval")
			no_cross_day = False
			break

	"""
	For simplicity, generate an output text file to include both the statistical information
	and each interval's information, also select the ones has >= X hour duration
	"""
	print ("\n\n============== generate output file ==============\n")
	
	f = open("dataset/ue_routes_infomation.txt", "w")
	
	f.write("chosen mnc = %s\n" %(chosen_mnc))
	f.write("time gap is set to be >= %f seconds\n" %(gap_in_sec))
	f.write("overall time interval: %s ~ %s\n" %(time_intervals[0].start_time, time_intervals[-1].end_time))
	f.write("total number of intervals = %d\n" %(total_number_intervals))
	f.write("interval length: max = %.2f sec, min = %.2f sec, avg = %.2f sec\n" 
						%(max(interval_length_list), min(interval_length_list), mean(interval_length_list)))
	f.write("interval gap: max = %.2f sec, min = %.2f sec, avg = %.2f sec\n"
						%(max(interval_gap_list), min(interval_gap_list), mean(interval_gap_list)))
	f.write("---------------------- list all intervals-----------------\n")


	################################# choose the consecutive intervals according to time bounds #########################################
	chosen_time_intervals = []

	for interval in time_intervals:
		f.write(str(interval.start_time))
		f.write(" ~ ")
		f.write(str(interval.end_time))
		f.write(", duration = < %s >\n" %str(interval.end_time-interval.start_time))

		# select the intervals that are >= X hours, to gain enough points to show
		time_difference = interval.end_time-interval.start_time
		if (time_difference.total_seconds() > total_sec_lb) and (time_difference.total_seconds() < total_sec_ub):
			f.write ("this one is chosen !!!!!!!!!!!!!\n")
			chosen_time_intervals.append(interval)

	print ("====== # intervals satisfy constrints = %d =======" %(len(chosen_time_intervals)))	
	
	f.close()

	""" ================= randomly pick an interval from all chosen ones ============= """
	tower_df = pd.read_csv('dataset/Working_tower_data.csv', usecols=['net', 'lat', 'lon', 'range'])
	tower_df = tower_df.loc[tower_df['net'] == chosen_mnc]
	# repeat this process until a valid route is chosen
	found_valid_route = False
	
	# these 3 data structures are keep updated in the while loop
	# if found a valid route, they are updated accordingly
	found_interval = None
	chosen_dlfreq = None

	selected_tower_records = []

	print ("================= start finding a valid route =====================")

	while not found_valid_route:

		found_valid_route = True

		if len(chosen_time_intervals) == 0:
			print ("ALL intervals tried, NOT working. Either change MNC or change time constraints of the route")
			sys.exit()

		found_interval = random.choice(chosen_time_intervals)
		chosen_time_intervals.remove(found_interval)

		found_interval_df = df.loc[(df['datetime'] >= found_interval.start_time) & 
									(df['datetime'] < found_interval.end_time)]

		#dates = pd.to_datetime(long_interval_df.datetime, format='%Y-%m-%d %H:%M:%S')
		#times = [d.time() for d in dates]

		#long_interval_df = (long_interval_df.assign(datetime=times)) 		# only keeps time, for NS3 to parse
		#long_interval_df.to_csv('dataset/random_long_interval.csv')

		# select the dlfreq with the max count in this dataframe
		dlfreq_list = found_interval_df['dlfreq'].to_list()
		chosen_dlfreq = max(set(dlfreq_list), key=dlfreq_list.count)

		found_interval_df  = found_interval_df[found_interval_df['dlfreq'] == chosen_dlfreq]

		# Now, perform sanity check using the time, distance and speed
		print ("\n")
		print ("---------- basic information of the found travel segment -------")
		print ("time: %s, duration = %s" %(found_interval, found_interval.end_time-found_interval.start_time))
	
		first_row = found_interval_df.iloc[0]
		last_row = found_interval_df.iloc[-1]
		diagnal_distance = geodesic((first_row['latitude'], first_row['longitude']), 
									(last_row['latitude'], last_row['longitude'])).km

		# print ("total diagnal distance (minimum): %s km" %(diagnal_distance))
		# speed_list = [int(i) for i in (long_interval_df['speed'].to_list())]
		# print ("min, max, avg speed (km/h) = %d, %d, %d" %(max(speed_list), min(speed_list), mean(speed_list)))

		#################### draw rectangle and perform preliminary selection of towers ##################
		"""
		print ("================= preliminary selection of towers in the rectangle ==========")

		lat_list = found_interval_df['latitude'].to_list()
		lon_list = found_interval_df['longitude'].to_list()

		min_lat = min(lat_list)
		max_lat = max(lat_list)
		min_lon = min(lon_list)
		max_lon = max(lon_list)

		point_1 = geometry.Point(min_lon, min_lat)
		point_2 = geometry.Point(max_lon, min_lat)
		point_3 = geometry.Point(min_lon, max_lat)
		point_4 = geometry.Point(max_lon, max_lat)

		route_polygon = geometry.Polygon([point_1, point_2, point_3, point_4]).convex_hull

		for index, row in tower_df.iterrows():
			point = geometry.Point(row['lon'], row['lat'])
			if point.within(route_polygon):
				selected_tower_records.append(tower_records(row['net'], row['lat'], row['lon'], row['range']))
		
		"""
		
		pci_group = found_interval_df.groupby(['pci'])
		pci_list = found_interval_df.pci.unique()

		"""
		requirements: (1) 2 or 3 PCI segments; 
					  (2) Each segment at least 5 measurements
					  (3) PCI points must be consecutive
					  (4) distance at least X meters
		"""
		if len(pci_list) < 2:
			found_valid_route = False

		for pci in pci_list:
			
			pci_df = pci_group.get_group(pci).reset_index()
			if len(pci_df) < 5:
				found_valid_route = False

		"""========================================================
		This part of code enforce the measurements to be "non-overalpping"
		"""
		
		seen_pci = []
		prev_pci = found_interval_df.iloc[0]['pci']
		seen_pci.append(prev_pci)

		for index, row in found_interval_df.iterrows():
			current_pci = row['pci']
			if current_pci != prev_pci:
				if current_pci in seen_pci:
					found_valid_route = False
					break
				else:
					seen_pci.append(current_pci)
					prev_pci = current_pci 
			else:
				pass
		
		""" ======================================================"""

		"""=======================================================
		This part of code enforce the diagnal distance must be greater than some value
		"""
		first_row = found_interval_df.iloc[0]
		last_row = found_interval_df.iloc[-1]

		if geodesic( (first_row['latitude'], first_row['longitude']),
					 (last_row['latitude'], last_row['longitude']) ).m < 50:
			found_valid_route = False
		"""======================================================"""

		if not found_valid_route:
			print ("not valid route, continue")
			selected_tower_records.clear()		
		else:
			print ("valid route found, continue to next step")

	###################### print information of the valid route #########################
	print ("===================================== found a valid route, print info ===============================")
	print ("the valid route time interval = %s" %(found_interval))
	print ("the chosen downlink frequency = %s" %(chosen_dlfreq))

	found_interval_df = df.loc[(df['datetime'] >= found_interval.start_time) & (df['datetime'] < found_interval.end_time)]
	
	# further process of the datetime, to exclude the "data" part
	datetimes = found_interval_df.datetime.to_list()
	datetimes = [d.time() for d in datetimes]
	found_interval_df = (found_interval_df.assign(datetime=datetimes))
	
	# then pick the rows with the dlfreq we need
	found_interval_df = found_interval_df[found_interval_df['dlfreq'] == chosen_dlfreq]
	found_interval_df.to_csv('dataset/final_valid_route.csv')

	print ("the corresponding dataframe: ")
	print (found_interval_df)

	######################################################
	"""
	selected_towers_df = pd.DataFrame(columns=['mnc', 'lat', 'lon', 'range', 'pci'])
	for tower in selected_tower_records:
		selected_towers_df = selected_towers_df.append(pd.Series({'mnc' : tower.mnc, 
															  	  'lat' : tower.lat, 
															  	  'lon' : tower.lon, 
															  	  'range' : tower.tower_range,
															  	  'pci' : '0'}), 
															  	  ignore_index=True)

	selected_towers_df.to_csv('dataset/selected_towers.csv')

	print ("=============== chosen towers df ===================")
	print (chosen_towers_df)
	chosen_towers_df.to_csv('dataset/chosen_towers.csv')
	
	################################## start drawing ##############################
	print ("====================== start drawing figures ========================")
	color_dict = {}

	for pci in pci_list:
		r = np.random.choice(range(256))
		g = np.random.choice(range(256))
		b = np.random.choice(range(256))
		color_dict[pci] = "#%02x%02x%02x" %(r, g, b) 	# store HEX format

	color_list = []
	for index, row in found_interval_df.iterrows():
		pci = row['pci']
		color_list.append(color_dict[pci])

	found_interval_df['color'] = color_list

	color_list_for_towers = []
	for index, row in chosen_towers_df.iterrows():
		pci = row['pci']
		color_list_for_towers.append(color_dict[pci])

	chosen_towers_df['color'] = color_list_for_towers

	print ("------------------- color column added to route df --------------------")
	print (found_interval_df)

	route_points = [geometry.Point(xy) for xy in zip(found_interval_df['longitude'], found_interval_df['latitude'])]
	geo_ue_df = gpd.GeoDataFrame(found_interval_df, crs=crs, geometry=route_points)

	chosen_tower_points = [geometry.Point(xy) for xy in zip(chosen_towers_df['lon'], chosen_towers_df['lat'])]
	geo_chosen_tower_df = gpd.GeoDataFrame(chosen_towers_df, crs=crs, geometry=chosen_tower_points)

	chosen_tower_circles = []
	for index, row in chosen_towers_df.iterrows():
		c = geodesic_point_buffer(row['lat'], row['lon'], row['range'])
		chosen_tower_circles.append(c)

	geo_chosen_tower_circle_df = gpd.GeoDataFrame(crs=crs, geometry=chosen_tower_circles)
	
	fix, ax = plt.subplots()

	geo_ue_df.plot(color=found_interval_df['color'], ax=ax, alpha=1, markersize=2, legend=True)
	#geo_selected_tower_df.plot(ax=ax, alpha=1, color='black', markersize=5)
	geo_chosen_tower_df.plot(color=chosen_towers_df['color'], ax=ax, alpha=1, markersize=5)
	#geo_chosen_tower_circle_df.plot(ax=ax, alpha=0.5, color='green', markersize=1)		# mark the chosen towers to be red

	plt.title('Route map with chosen towers', fontsize=15)
	plt.savefig("figures/valid_route_with_towers.png")

	plt.clf()
	"""