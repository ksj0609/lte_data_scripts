
	""" ============= then, process the routes according to MNC =================="""
	
	# (1) group the dataframes by MNC
	#mnc_values = long_interval_df.mnc.unique()
	#mnc_groups = long_interval_df.groupby(['mnc'])

	# select a random mnc group as an example
	#random_mnc = random.choice(mnc_values)
	#random_mnc_group = mnc_groups.get_group(random_mnc).reset_index()

	#print ("--------------- group key = %s ------------" %(random_mnc))
	#print (random_mnc_group)

	#random_mnc_group.to_csv('dataset/mnc_specific_group_route.csv')

	# (2) select a downlink frequency value to be used, the one with most appearance
	#dlfreq_list = random_mnc_group['dlfreq'].to_list()
	#chosen_dlfreq = max(set(dlfreq_list), key=dlfreq_list.count)

	#print (dlfreq_list)
	print ("====================== chosen dlfreq = %s =========================" %(chosen_dlfreq))

	random_mnc_dlfreq_group = random_mnc_group.loc[random_mnc_group['dlfreq'] == chosen_dlfreq]
	random_mnc_dlfreq_group.to_csv('dataset/mnc_dlfreq_specific_group_route.csv')

	# (3) partition the routes into segments, could it simply group by "pci"?
	# keep in mind that the PCI represents eNBs
	# we found one PCI can corresponds to multiple segments within the same MNC
	# how ever, since the measurement only records "best signal", the continuous
	# PCIs are dissected apart (still same eNB but discontinued), how to deal with it?

	# for simplicity here, we just group the route by PCI values, then find the ones
	# that are too far apart (belongs to different eNBs)
	
	print ("==================== group chosen mnc-dl route by pci values ===========================")

	random_mnc_group_with_pci_partition = random_mnc_dlfreq_group.groupby(['pci'])
	random_mnc_group_pci_list = random_mnc_dlfreq_group.pci.unique()

	# construct "color column" in dataframe, according to PCI values
	color_dict = {} 		# key = pci, value = random color RGB	
	for pci in random_mnc_group_pci_list:
		r = np.random.choice(range(256))
		g = np.random.choice(range(256))
		b = np.random.choice(range(256))
		color_dict[pci] = "#%02x%02x%02x" %(r, g, b) 	# store HEX format

	print (color_dict)

	color_list = [] 		# then assign each row with the color value
	for index, row in random_mnc_dlfreq_group.iterrows():
		pci = row['pci']
		color_list.append(color_dict[pci])

	print ("size of dataframe = %d, size of color list = %d" %(len(random_mnc_dlfreq_group), len(color_list)))
	
	assert(len(random_mnc_dlfreq_group) == len(color_list))

	random_mnc_dlfreq_group['color'] = color_list

	print ("===================== print dataframe after color added =============")
	print (random_mnc_dlfreq_group)

	# draw routing points with pci colors
	route_points = [geometry.Point(xy) for xy in zip(random_mnc_dlfreq_group['longitude'], random_mnc_dlfreq_group['latitude'])]
	geo_ue_df = gpd.GeoDataFrame(random_mnc_dlfreq_group, crs=crs, geometry=route_points)

	print ("================== print_geo_ue_df =====================")
	print (geo_ue_df)

	fig, ax = plt.subplots()

	geo_ue_df.plot(color=random_mnc_dlfreq_group['color'], ax=ax, alpha=1, markersize=2, legend=True)

	plt.title('Route map with PCI values', fontsize=15)
	plt.savefig("figures/random_long_route_1.png")
	
	plt.clf()

	# (4) process the tower data, first select the ones with the same MNC, then
	# futher filter the points that ONLY within the rectangle of the route

	# the rectangle is represented by the points <max lat, min lat, max lon, min lon>
	lat_list = random_mnc_dlfreq_group['latitude'].to_list()
	lon_list = random_mnc_dlfreq_group['longitude'].to_list()

	min_lat = min(lat_list)
	max_lat = max(lat_list)
	min_lon = min(lon_list)
	max_lon = max(lon_list)

	point_1 = geometry.Point(min_lon, min_lat)
	point_2 = geometry.Point(max_lon, min_lat)
	point_3 = geometry.Point(min_lon, max_lat)
	point_4 = geometry.Point(max_lon, max_lat)

	route_polygon = geometry.Polygon([point_1, point_2, point_3, point_4]).convex_hull


	# then, roughly filter the related points that are within the "route rectangle"
	# ToDo: here the rectangle should be further "refined" by select the MNC related points of UE

	selected_tower_points = [] 			# shapely Point list
	selected_tower_records = []			# class tower_record list

	print ("==================== select towers within the range =======================")

	for index, row in tower_df.iterrows():
		point = geometry.Point(row['lon'], row['lat'])
		if point.within(route_polygon):
			selected_tower_points.append(point)
			selected_tower_records.append(tower_records(row['net'], row['lat'], row['lon'], row['range']))

	# then, identify the towers that are outside the rectangle but intersect with it
	# ToDo

	geo_selected_tower_df = gpd.GeoDataFrame(crs=crs, geometry=selected_tower_points)

	fig, ax = plt.subplots()
	
	geo_ue_df.plot(color=random_mnc_dlfreq_group['color'], ax=ax, alpha=1, markersize=2, legend=True)
	geo_selected_tower_df.plot(ax=ax, alpha=1, color='black', markersize=5)
	
	plt.title('Route map with candidate towers', fontsize=15)
	plt.savefig("figures/random_long_route_2.png")
	
	plt.clf()

	# (5) for each PCI "segment", from the selected tower points, choose the ones that
	# cover ALL the points of this segment
	# candidates are stored as dict, with <key=PCI, value=list of selected tower records>

	candidate_tower_for_segments = {}
	frames = [] 		# this frames is used to re-order UE DataFrames according to PCI
						# as the original one is ordered by timestamp
						# this may NOT be our input for the NS3 experiments
	
	print ("==================== for each pci segment, choose towers ==================")
	"""
	Problem: there exist cell towers that covers multiple PCI segments, thus the "covering towers"
	will have overlapped towers (their PCI values are assigned multiple times)

	How to deal with it?
	"""
	
	for pci in random_mnc_group_pci_list:

		print ("choose towers that cover segment with pci = %s" %(str(pci)))
		
		pci_df = random_mnc_group_with_pci_partition.get_group(pci).reset_index()
		frames.append(pci_df)

		#lat_list = pci_df.latitude.to_list()
		#lon_list = pci_df.longitude.to_list()
		#points = [geometry.Point(xy) for xy in zip(pci_df['latitude'], pci_df['longitude'])]

		max_candidates = 2		# to avoid picking ALL available towers
								# constrain the number for each PCI segment to be X
		counter = 1 			

		covering_towers = [] 	# now select the towers that cover all the points of this PCI segment
		
		for tower_record in selected_tower_records:

			print ("\tcheck tower record: %s" %(tower_record))
			cover = True

			for index, row in pci_df.iterrows():
				if geodesic((row['latitude'], row['longitude']),
							(tower_record.lat, tower_record.lon)).m > float(tower_record.tower_range):
					cover = False
					print ("\tthis tower does NOT cover at least one point, jump to next tower")
					break

			if cover: 	# this tower cover all the segment points

				print ("\tthis tower covers ALL points in the second, add this to list and remove")
				tower_record.pci = str(pci)
				tower_record.dlfreq = chosen_dlfreq
				covering_towers.append(tower_record)
				selected_tower_records.remove(tower_record)

				# if counter exceeds max_candidates
				if counter == max_candidates:
					print ("\tselected %d towers already, jump to next segment" %(counter))
					counter = 1
					break
				else:
					print ("\thaven't reach maximum, looking for next tower candidate")
					counter += 1			

		print ("chosen towers for pci = %s" %(str(pci)))
		for c in covering_towers:
			print (c)
		print ("-------------------")

		candidate_tower_for_segments[str(pci)] = covering_towers

	combined_pci_df = pd.concat(frames)
	combined_pci_df.to_csv('dataset/route_pci_data.csv')

	"""
	for index, value in candidate_tower_for_segments.items():
		print ("=============== candidate towers for pci = %s ===============" %(index))
		if len(value) == 0:
			print ("None")
		else:
			for r in value:
				print (r)
	"""

	"""
	f = open('dataset/pci_tower.json', 'w')
	json.dump(candidate_tower_for_segments, f, cls=tower_record_encoder, indent=2)
	f.close()
	"""

	# ToDo: select the "appropriate tower" for each segment, currently just randomly pick one
	# Then store as a sepearate CSV
	print ("========================== now for each segment, randomly pick one tower =================")

	chosen_towers_df = pd.DataFrame(columns=['mnc', 'lat', 'lon', 'range', 'pci'])
	
	for pci in random_mnc_group_pci_list: 	# use this order is important!!!
		
		print ("randomly pick towers for pci = %s" %(str(pci)))

		towers = candidate_tower_for_segments[str(pci)]
		
		if len(towers) != 0:
			chosen_tower_record = random.choice(towers)
			print ("candidates exist, randomly choose one: ")
			print (chosen_tower_record)			
			chosen_towers_df = chosen_towers_df.append(pd.Series({'mnc' : chosen_tower_record.mnc, 
																  'lat' : chosen_tower_record.lat, 
																  'lon' : chosen_tower_record.lon, 
																  'range' : chosen_tower_record.tower_range,
																  'pci' : chosen_tower_record.pci}), 
																  ignore_index=True)
		else:
			print ("candidates does NOT exist")

	print ("=============== chosen towers df ===================")
	print (chosen_towers_df)
	chosen_towers_df.to_csv('dataset/chosen_towers.csv')

	""" ==== Then, draw everything, including the map, route, route rectangle, and towers ======== """

	chosen_tower_points = []
	chosen_tower_circles = []

	"""
	for index, row in chosen_towers_df.iterrows():
		p = geometry.Point(row['lon'], row['lat'])
		chosen_tower_points.append(p)
		#c = p
		#chosen_tower_circles.append(c.buffer(0.01).exterior)
	"""

	for index, row in chosen_towers_df.iterrows():
		p = geometry.Point(row['lon'], row['lat'])
		chosen_tower_points.append(p)	
		
	
	for index, row in chosen_towers_df.iterrows():
		c = geodesic_point_buffer(row['lat'], row['lon'], row['range'])
		chosen_tower_circles.append(c)
	
	geo_chosen_tower_point_df = gpd.GeoDataFrame(crs=crs, geometry=chosen_tower_points)
	geo_chosen_tower_circle_df = gpd.GeoDataFrame(crs=crs, geometry=chosen_tower_circles)

	#route_polygons = [route_polygon]
	#geo_route_df = gpd.GeoDataFrame(crs=crs, geometry=route_polygons) 		# plot the polygon surrounding the route 

	fig, ax = plt.subplots()

	geo_ue_df.plot(color=random_mnc_dlfreq_group['color'], ax=ax, alpha=1, markersize=2, legend=True)
	geo_selected_tower_df.plot(ax=ax, alpha=1, color='black', markersize=5)

	geo_chosen_tower_circle_df.plot(ax=ax, alpha=0.5, color='green', markersize=1)		# mark the chosen towers to be red
	geo_chosen_tower_point_df.plot(ax=ax, alpha=1, color='red', markersize=8)

	plt.title('Route map with chosen towers', fontsize=15)
	plt.savefig("figures/random_long_route_3.png")

	plt.clf()

	"""
	# import UK map file, read by Geopandas and return as a GeoDataFrame
	geo_map_df = gpd.read_file("maps/UnitedKingdom_Bound.shp") 		# load UK map as GeoDataFrame

	fig, ax = plt.subplots()

	geo_map_df.plot(ax=ax, alpha=1,color='grey') 	# GeoDataFrame.plot() function
	geo_ue_df.plot(ax=ax, alpha=1, color='blue', markersize=0.5)
	geo_tower_df.plot(ax=ax, alpha=1, color='green', markersize=0.5)

	plt.title('Random long route map', fontsize=15)
	plt.savefig("figures/random_long_route_with_map.png")
	"""