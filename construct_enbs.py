#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import random
import json
import pyproj
import math
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd

from functools import partial
from json import JSONEncoder
from datetime import datetime
from statistics import mean
from geopy.distance import geodesic

from matplotlib import cm
from shapely import geometry
from shapely import ops

"""
A separate estimation algorithm for locate eNBs for each PCI segment
However, don't limit the candidates to the corresponding towers with the same PCI, which
means the search grid for EACH PCI includes ALL towers. But for simplicity, the "reference"
eNB location to identify p_0 is chosen to be the first eNB with the same segment's PCI value
"""

class cartesian_coords:
	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z

	def __str__(self):
		return "<%s, %s, %s>" %(self.x, self.y, self.z)

# this function just copied from NS3
def geo_to_cartesian(lat, lon, alt):

	a = 6378137
	e = 0.0818191908426215

	latitudeRadians = 0.01745329 * lat
	longitudeRadians = 0.01745329 * lon

	Rn = a / (math.sqrt (1 - math.pow(e, 2)*math.pow(math.sin(latitudeRadians), 2)))
	x = (Rn + alt) * math.cos (latitudeRadians) * math.cos (longitudeRadians)
	y = (Rn + alt) * math.cos (latitudeRadians) * math.sin (longitudeRadians)
	z = ((1 - math.pow (e, 2)) * Rn + alt) * math.sin (latitudeRadians)
	return cartesian_coords(x, y, z)

proj_wgs84 = pyproj.Proj(init='epsg:4326')
def geodesic_point_buffer(lat, lon, m):

	# Azimuthal equidistant projection
	aeqd_proj = '+proj=aeqd +lat_0={lat} +lon_0={lon} +x_0=0 +y_0=0'
	project = partial(
		pyproj.transform,
		pyproj.Proj(aeqd_proj.format(lat=lat, lon=lon)),
		proj_wgs84)

	buf = geometry.Point(0, 0).buffer(m)  # distance in metres
	return ops.transform(project, buf).exterior

class tower_record:
	def __init__(self, lat, lon):
		self.lat = lat
		self.lon = lon
		self.pci = "0"
		self.tx_power = 0.0
	
	def __init__(self, lat, lon, pci):
		self.lat = lat
		self.lon = lon
		self.pci = pci
		self.tx_power = 0.0

	def __str__(self):
		return "<lat = %s, lon = %s, pci = %s, tx_power = %f>" %(self.lat, self.lon, self.pci, self.tx_power)

if __name__ == '__main__':

	############# all the arguments past to the main function ############# 
	route_in_file = sys.argv[1]	 				# input train route csv file
	route_out_file = sys.argv[2]				# output of the modified train route file
	tower_out_file = sys.argv[3]				# output of the constructed tower csv file
	route_figure_file = sys.argv[4]				# output of the route and the figure
	rsrp_figure_file = sys.argv[5]				# output of the rsrp difference figure
	r_coef = float(sys.argv[6])					# the radius of the circule = coefficient * R
	#######################################################################

	crs = "epsg:4326" 	# designate coordinate system

	ue_df = pd.read_csv(route_in_file)

	"""#################################################################
	This part should be in another python script
	This snippet is to modify the "speed" column to make sure ALL the locations can be
	correctly reached at the specified time

	The assumption here is that the train travels through the shortest path and constant speed
	Since we don't know the actual situation, this is the best we could do
	"""
	dates = pd.to_datetime(ue_df.datetime, format='%H:%M:%S')
	
	modified_speed = []
	for index, row in ue_df.iterrows():
		if index != len(ue_df)-1:
			next_row = ue_df.iloc[index+1]
			distance = geodesic((row['latitude'], row['longitude']), 
								(next_row['latitude'], next_row['longitude'])).m
			
			time_diff_sec = (dates[index+1]-dates[index]).total_seconds()
			print ("time diff in sec = %f" %(time_diff_sec))
			speed = 0
			if time_diff_sec != 0:
				speed = distance / time_diff_sec
			print ("modified speed = %f" %(speed))
			modified_speed.append(speed)

	modified_speed.append(0.0)
	ue_df['speed'] = modified_speed

	""" #################################################################"""
	
	dlfreq = float(ue_df.iloc[0]['dlfreq'])
	#print ("\n---------------------------------------------------------")
	#print ("downlink frequency = %f MHz" %(dlfreq))
	
	const_loss = 20*math.log10((4*math.pi*dlfreq*math.pow(10, 6)) / (3*math.pow(10, 8)))
	#print ("constant lost = %f dBm" %(const_loss))
	#print ("-----------------------------------------------------------\n")

	pci_list = ue_df.pci.unique()
	ue_pci_group = ue_df.groupby(['pci'])

	for pci in pci_list:
		pci_df = ue_pci_group.get_group(pci).reset_index()
	
	# estimate p0 for each pci segment
	# assume alpha is already set to be 2
	# then p0 is just the average value of the difference between actual rsrp and estimate rsrp
	all_tower_records = []
	chosen_tower_records = []

	estimated_rsrp = [] 		# this is the estimated rx_power after identified each eNB

	for pci in pci_list:

		print ("-------------- find eNB for pci = %s --------------" %(pci))
		pci_df = ue_pci_group.get_group(pci).reset_index()

		# construct candidate eNBs for this PCI
		candidate_tower_records = []

		lat_list = pci_df['latitude'].to_list()
		lon_list = pci_df['longitude'].to_list()

		radius = geodesic((min(lat_list), min(lon_list)), 
											(max(lat_list), max(lon_list))).m / 2

		print ("radius = %f" %(radius))

		mid_lat = np.mean(lat_list)
		mid_lon = np.mean(lon_list)

		circle_candidates = geodesic_point_buffer(mid_lat, mid_lon, radius*r_coef)
		
		for coord in circle_candidates.coords:
			all_tower_records.append(tower_record(coord[1], coord[0], pci))
			candidate_tower_records.append(tower_record(coord[1], coord[0], pci))

		candidate_tower_mean = []
		candidate_tower_var = []

		# since we already know the dl frequency, the term with (4*pi*f)/c is calculated
		for tower in candidate_tower_records:
			
			#print ("checking tower record %s: " %(tower))
						
			estimate_error_list = []
			
			for index, row in pci_df.iterrows():
				d = geodesic((tower.lat, tower.lon) , (row['latitude'], row['longitude'])).m
				#print ("\tdistance to pci <%s, %s> = %f" %(row['latitude'], row['longitude'], d))
				total_lost = 20*math.log10(d)+const_loss
				#print ("\ttotal lost = %f" %(total_lost))
				error_plus_p0 = row['cal_rsrp']+total_lost
				#print ("\terror_plus_p0 = %f" %(error_plus_p0))
				estimate_error_list.append(error_plus_p0)

			print ("error list = %s" %(str(estimate_error_list)))

			mean = np.mean(estimate_error_list)
			var = np.var(estimate_error_list)

			#print ("this tower has mean = %f, var = %f" %(mean, var))
			candidate_tower_mean.append(mean)
			candidate_tower_var.append(var)

		# choose the tower candidate with the minimum 
		chosen_id = candidate_tower_var.index(min(candidate_tower_var))
		
		chosen_tower = candidate_tower_records[chosen_id]
		chosen_tower_mean = candidate_tower_mean[chosen_id]
		chosen_tower.tx_power = chosen_tower_mean
		chosen_tower_records.append(chosen_tower)

		#print ("--------- The chosen tower for pci = %s is: %s ----------" %(pci, chosen_tower))
		
		# here calculate the estimated power loss, given the chosen tower
		# then save it to the output csv file
		for index, row in pci_df.iterrows():
			d = geodesic((chosen_tower.lat, chosen_tower.lon) , (row['latitude'], row['longitude'])).m
			power_loss = const_loss+20*math.log10(d)
			rx_power = chosen_tower.tx_power-power_loss

			estimated_rsrp.append(rx_power)	

			lat = float(row['latitude'])
			lon = float(row['longitude'])
			coord = geo_to_cartesian(lat, lon, 0.0)
	
	ue_df['est_rsrp'] = estimated_rsrp
	ue_df.to_csv(route_out_file, index=False)		

	# store the chosen towers into DataFrames and save to csv file
	chosen_towers_df = pd.DataFrame(columns=['mnc', 'lat', 'lon', 'tx_power', 'pci'])

	for chosen_tower in chosen_tower_records:
		chosen_towers_df = chosen_towers_df.append(pd.Series({'mnc' : '0', 
															  'lat' : chosen_tower.lat, 
															  'lon' : chosen_tower.lon, 
															  'tx_power' : chosen_tower.tx_power,
															  'pci' : chosen_tower.pci}), 
															  ignore_index=True)

	chosen_towers_df.to_csv(tower_out_file)

	# all tower candidates for visualization
	# this don't need to save, just draw figures
	all_towers_df = pd.DataFrame(columns=['lat', 'lon', 'pci'])
	for t in all_tower_records:
		all_towers_df = all_towers_df.append(pd.Series({'mnc' : '0', 
														'lat' : t.lat, 
														'lon' : t.lon, 
														'pci' : t.pci}), 
														ignore_index=True)
	
	########################################### drawing #####################################################
	print ("====================== start drawing figures ========================")
	
	# PCI mapped colors, with key = pci, value = color in HEX format
	color_dict = {}

	for pci in pci_list:
		r = np.random.choice(range(256))
		g = np.random.choice(range(256))
		b = np.random.choice(range(256))
		color_dict[pci] = "#%02x%02x%02x" %(r, g, b) 	# store HEX format

	# color list is the color for each train route point
	ue_color_list = []
	for index, row in ue_df.iterrows():
		pci = row['pci']
		ue_color_list.append(color_dict[pci])

	ue_df['color'] = ue_color_list
	route_points = [geometry.Point(xy) for xy in zip(ue_df['longitude'], ue_df['latitude'])]
	geo_ue_df = gpd.GeoDataFrame(ue_df, crs=crs, geometry=route_points)

	# tower color list is the color of candidate towers for each PCI segments
	tower_color_list = []
	for index, row in all_towers_df.iterrows():
		pci = row['pci']
		tower_color_list.append(color_dict[pci])

	all_towers_df['color'] = tower_color_list
	tower_points = [geometry.Point(xy) for xy in zip(all_towers_df['lon'], all_towers_df['lat'])]
	geo_all_towers_df = gpd.GeoDataFrame(all_towers_df, crs=crs, geometry=tower_points)

	chosen_tower_color_list = []
	for index, row in chosen_towers_df.iterrows():
		pci = row['pci']
		chosen_tower_color_list.append(color_dict[pci])

	chosen_towers_df['color'] = chosen_tower_color_list
	chosen_tower_points = [geometry.Point(xy) for xy in zip(chosen_towers_df['lon'], chosen_towers_df['lat'])]
	geo_chosen_towers_df = gpd.GeoDataFrame(chosen_towers_df, crs=crs, geometry=chosen_tower_points)

	##################################################################
	fix, ax = plt.subplots()

	geo_ue_df.plot(color=ue_df['color'], ax=ax, alpha=1, markersize=5)
	geo_all_towers_df.plot(color='black', ax=ax, alpha=1, markersize=1)
	geo_chosen_towers_df.plot(color='red', ax=ax, alpha=1, markersize=10)
	
	plt.title('Route map with chosen towers', fontsize=10)
	plt.savefig(route_figure_file)

	plt.clf()
	
	#################################################################
	fix, ax = plt.subplots()
	ax.plot(ue_df.cal_rsrp.to_list(), color='blue', label='real')
	ax.plot(estimated_rsrp, color='red', label='estimated')
	ax.legend()

	plt.savefig(rsrp_figure_file)
	plt.clf()


























