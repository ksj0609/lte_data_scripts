#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import geopandas as gpd

from shapely.geometry import Point, Polygon
from datetime import datetime 

"""
	Collect the speed statistics of the UE data:
		1. Categorize dataframe into N bins:
			(1) overall
			(2) speed = 0
			(3) speed = 1~50
			(4) speed = 51~100
			(5) speed = 101~150
			...

		2. For each bin:
			(1) number of records 					// pie chart
			(2) RSRP, RSRQ, SINR 					// violin plot

"""
def make_autopct(values):
	def my_autopct(pct):
		total = sum(values)
		val = int(round(pct*total/100.0))
		return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
	return my_autopct


if __name__ == '__main__':
	
	df = pd.read_csv('lte-jun18tojun19-yt.csv', usecols=['speed', 'cal_rsrp', 'rsrq', 'sinr'])
	
	df.dropna(axis=0, how='any', inplace=True) 	# drop the rows that contains ANY column value to be nan
												# do it inplace

	# split non-zero dataframes into the bins with delta speed = 50
	# i.e., [0, 1~50, 51~100, ...]
	max_speed_value = df['speed'].max()
	print ("max speed = %d" %(max_speed_value))

	df_zeros = df[df['speed'] == 0]
	df_non_zeros = []

	s = 1
	while s+50 < int(max_speed_value):
		print ("add dataframe with speed from %d to %d" %(s, s+50-1))		
		df_non_zeros.append(df[df['speed'].astype(int).between(s, s+50, inclusive=True)])
		s += 50

	print ("add dataframe with speed from %d to %d" %(s, max_speed_value))
	df_non_zeros.append(df[df['speed'].astype(int).between(s, max_speed_value, inclusive=True)])

	fig, axes = plt.subplots()

	############################ count chart ################################
	sizes = []
	sizes.append(len(df_zeros))
	for d in df_non_zeros:
		sizes.append(len(d))

	labels = []
	labels.append('Zero')
	
	s = 1
	for _ in range(1, len(df_non_zeros)):
		labels.append("%d~%d" %(s, s+50-1))
		s += 50
	
	labels.append("%d~%d" %(s, max_speed_value))

	print (labels)

	#axes.pie(sizes, labels=labels, shadow=True, autopct=make_autopct(sizes))

	axes.pie(sizes, labels=labels, shadow=True, autopct='%.2f%%')

	
	#draw circle
	centre_circle = plt.Circle((0,0),0.70,fc='white')
	fig = plt.gcf()
	fig.gca().add_artist(centre_circle)
	

	axes.axis('equal')
	axes.set_title("Total # of measurements = %d" %(len(df)))
	plt.savefig("number_of_counts.png")
	plt.clf()

	
	############################## rsrq distribution ##########################################
	labels.insert(0, 'Overall')

	fig, axes = plt.subplots()

	rsrq_values = []
	rsrq_values.append(df['rsrq'].values)
	rsrq_values.append(df_zeros['rsrq'].values)

	for d in df_non_zeros:
		rsrq_values.append(d['rsrq'].values)

	axes.violinplot(dataset=rsrq_values, showmeans=True)

	axes.yaxis.grid(True)
	#axes.set_xlabel('Group')
	axes.set_ylabel('RSRQ (dB)')
	axes.set_title('RSRQ plot')

	#labels = ['Overall', 'Zero', 'Non-zero']
	axes.set_xticks(np.arange(1, len(labels) + 1))
	axes.set_xticklabels(labels)
	plt.savefig("rsrq_distribution.png")
	axes.cla()

	############################### rsrp distribution #########################################
	rsrp_values = []
	rsrp_values.append(df['cal_rsrp'].values)
	rsrp_values.append(df_zeros['cal_rsrp'].values)

	for d in df_non_zeros:
		rsrp_values.append(d['cal_rsrp'].values)

	axes.violinplot(dataset=rsrp_values, showmeans=True)

	axes.yaxis.grid(True)
	#axes.set_xlabel('Group')
	axes.set_ylabel('RSRP (dBm)')
	axes.set_title('RSRP plot')

	axes.set_xticks(np.arange(1, len(labels) + 1))
	axes.set_xticklabels(labels)

	plt.savefig("rsrp_distribution.png")
	axes.cla()

	################################# sinr distribution ######################################
	sinr_values = []
	sinr_values.append(df['sinr'].values)
	sinr_values.append(df_zeros['sinr'].values)

	for d in df_non_zeros:
		sinr_values.append(d['sinr'].values)

	print (len(sinr_values))

	axes.violinplot(dataset=sinr_values, showmeans=True)

	axes.yaxis.grid(True)
	#axes.set_xlabel('Group')
	axes.set_ylabel('SINR (dB)')
	axes.set_title('SINR plot')

	axes.set_xticks(np.arange(1, len(labels) + 1))
	axes.set_xticklabels(labels)

	plt.savefig("sinr_distribution.png")
	axes.cla()
	
	