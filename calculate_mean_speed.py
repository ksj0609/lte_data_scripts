#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import statistics
import pandas as pd
from os import listdir

"""
Read every route file and generate 
(1) the modified route file
(2) the construcuted eNB csv file
(3) route figure
(4) rsrp figure
"""

if __name__ == '__main__':

	file_dir = "dataset/results/"
	
	total_speed = []

	for i in range(0, 555):
		print ("----------- reading file %d -------------" %(i))
		file_name = file_dir+"route_%d.csv" %(i)
		df = pd.read_csv(file_name)
		for index, row in df.iterrows():
			speed = float(row['speed'])
			total_speed.append(speed)

	#print (str(total_speed))

	print ("mean = %f, std = %f" %(statistics.mean(total_speed), statistics.pstdev(total_speed)))



