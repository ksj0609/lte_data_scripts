#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
from os import listdir

"""
Read every route file and generate 
(1) the modified route file
(2) the construcuted eNB csv file
(3) route figure
(4) rsrp figure
"""

if __name__ == '__main__':

	input_file_dir = "dataset/test_routes/" 				# file directory to read the route file
	output_file_dir = "dataset/test_routes/"		# file directory to store all the output, can be the same

	tower_range = 1											# coefficient of tower radius 									

	#print (listdir(input_file_dir))

	file_list = listdir(input_file_dir)
	
	for f in file_list:
	
		print (f)
		s = f.split('.')[0]
		s = s.split('_')[1]

		in_file = input_file_dir+"route_"+s+".csv" 			# route file to read
		out_route = output_file_dir+"route_"+s+".csv" 		# route file to write, add the "est_rsrp" column
		out_enb = output_file_dir+"enb_"+s+".csv"
		out_enb_fig = output_file_dir+"route_vis_"+s+".png"
		out_rsrp_fig = output_file_dir+"rsrp_est_error_"+s+".png"

		subprocess.run(['./construct_enbs.py %s %s %s %s %s %s' 
			%(in_file, out_route, out_enb, out_enb_fig, out_rsrp_fig, str(tower_range))], shell=True)
	

